/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */

class PowerPlant {
  constructor() {
    this.isPowered = true;
  }
}
class Household {
  constructor() {
    this.connections = new Set();
  }
}

export class World {
  constructor() {
    this.verifiedConnections = new Set();
  }

  createPowerPlant() {
    return new PowerPlant();
  }

  createHousehold() {
    return new Household();
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    household.connections.add(powerPlant);
  }

  connectHouseholdToHousehold(household1, household2) {
    household1.connections.add(household2);
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    household.connections.delete(powerPlant);
  }

  killPowerPlant(powerPlant) {
    powerPlant.isPowered = false;
  }

  repairPowerPlant(powerPlant) {
    powerPlant.isPowered = true;
  }

  hasPowered(connections) {
    return [...connections].some((connect) => {
      if (!this.verifiedConnections.has(connect)) {
        this.verifiedConnections.add(connect);
        if (connect instanceof Household) {
          return this.hasPowered(connect.connections);
        }
        return connect.isPowered;
      }
    });
  }

  householdHasEletricity(household) {
    this.verifiedConnections.clear();
    return this.hasPowered(household.connections);
  }
}
